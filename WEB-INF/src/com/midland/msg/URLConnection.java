package com.midland.msg;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class URLConnection {

	private final String USER_AGENT = "Mozilla/5.0";

	/*
	// HTTP GET request
	public void sendGet(String subject,String summary, String bc_code,String to_emp_id,String userName) throws Exception {
//		 subject = URLEncoder.encode(subject, "UTF-8").replace("+", "%20").replace("%2F", "/").replace("\n","%20");
//		 summary = URLEncoder.encode(summary, "UTF-8").replace("+", "%20").replace("%2F", "/").replace("\n","%20");
		subject = URLEncoder.encode(subject, "UTF-8");
		 summary = URLEncoder.encode(summary, "UTF-8");
			
		String url ="http://api-dev.midland.com.hk/salesKit/system_message.jsp?subject="+subject+"&summary="+summary+"&bc_code="+bc_code+"&to_emp_id="+to_emp_id+"&userName="+userName;
//		System.out.println(url);
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
int i=0;
		// optional default is GET
		conn.setRequestMethod("GET");

		//add request header
		conn.setRequestProperty("User-Agent", USER_AGENT);
		conn.setRequestProperty("charset", "UTF-8");

		int responseCode = conn.getResponseCode();
//		System.out.println("\nSending 'GET' request to URL : " + url);
//		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(conn.getInputStream(),"UTF-8"));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
//		System.out.println(response.toString());
		
	}
	*/
	
	// HTTP POST request
	public void sendPost(String subject,String summary, String bc_code,String to_emp_id,String userName) throws Exception {
		try{      
			//	String url = "http://api-dev.midland.com.hk/salesKit/system_message.jsp";
			String url = "http://api.midland.com.hk/salesKit/system_message.jsp";
			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setRequestProperty("Accept-Language", "UTF-8");

			subject = URLEncoder.encode(subject, "UTF-8");
			summary = URLEncoder.encode(summary, "UTF-8");
			userName = URLEncoder.encode(userName, "UTF-8");

			String urlParameters = "subject="+subject+"&summary="+summary+"&bc_code="+bc_code+"&to_emp_id="+to_emp_id+"&userName="+userName;

//			String urlParameters = "subject=subjecttest&summary=summarytest&bc_code=IT_SYS&to_emp_id=H1521962&userName=userNametest";
				
			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();
	//		System.out.println("\nSending 'POST' request to URL : " + url);
	//		System.out.println("Post parameters : " + urlParameters);
	//		System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(conn.getInputStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();


			//print result
//			System.out.println("response.toString()");	
			System.out.println(response.toString());

		}
		catch(Exception e) {
				System.out.println("sendPost");	
				e.printStackTrace();
		}
		finally {

		}
	}
	
	public void sendPostToIT(String subject,String summary, String bc_code,String to_emp_id,String userName) throws Exception {
		try{
			String url = "http://api-dev.midland.com.hk/salesKit/system_message.jsp";

			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setRequestProperty("Accept-Language", "UTF-8");

			subject = URLEncoder.encode(subject, "UTF-8");
			summary = URLEncoder.encode(summary, "UTF-8");
			userName = URLEncoder.encode(userName, "UTF-8");

			String urlParameters = "subject="+subject+"&summary="+summary+"&bc_code="+bc_code+"&to_emp_id="+to_emp_id+"&userName="+userName;

//			String urlParameters = "subject=subjecttest&summary=summarytest&bc_code=IT_SYS&to_emp_id=H1521962&userName=userNametest";
		
		
			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();
	//		System.out.println("\nSending 'POST' request to URL : " + url);
	//		System.out.println("Post parameters : " + urlParameters);
	//		System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(conn.getInputStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();


			//print result
//			System.out.println("response.toString()");	
			System.out.println(response.toString());

		}
		catch(Exception e) {
				System.out.println("sendPost");	
				e.printStackTrace();
		}
		finally {

		}
	}
}