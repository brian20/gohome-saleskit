package com.midland.msg;

import java.util.*;
import java.sql.*;
import java.lang.StringBuffer;

import javax.naming.*;
import javax.sql.*;

public class Msg {
	private Locale currentLocale = new Locale("zh", "HK");

	public Locale getCurrentLocale() {
		return this.currentLocale;
	}

	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}
	private boolean isPro = true;
	
	public void sendToSales() {
		
		PreparedStatement ps = null;
		ResultSet rs = null;	
		Connection conn = null;
	
	
		try {

			URLConnection http = new URLConnection();
			
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");
			
			String query = 
					"select su.emp_id, g.create_date, s.stock_id serial_no, g.gohome_serial_no, s.tx_type, s.region_chi_name, s.dist_chi_name, " + 
				    "s.est_chi_name, s.phase_chi_name, s.bldg_chi_name, s.floor, s.flat " +
					"from gohome_stock g " +
					"inner join stock s on g.serial_no = s.serial_no " +
					"inner join stock_agents su on su.serial_no = s.serial_no " + 
					"inner join ( " + 
					"	 select serial_no, min(post_type_seq) post_type_seq from stock_agents " + 
					"	 group by serial_no " + 
					") t on t.serial_no = g.serial_no and t.post_type_seq = su.post_type_seq " + 
					"where g.msg_sent_date is null " +
					"	and g.gohome_serial_no is not null " +
					"and g.gohome_unpub_date is null "+
					"order by su.emp_id desc ";
			
				conn = dataSource.getConnection();
				ps = conn.prepareStatement(query);			
				rs = ps.executeQuery();		
		
							
				String temp_emp_id="";
				String tempSummary="";
				String perStockDetail="";
				String finalSummary="";
				String subject ="GoHome 分類廣告";
				String bc_code="MKT" ;
				String emp_id="";
				String sqlSerial_no="";
				String userName="市場部";
				String sendToIT = "H0422923";

				System.out.println("sendToSales start ");		


				 int i =0;
				  while(rs.next()){
					  emp_id = rs.getString("emp_id");				
						String serialNumber = rs.getString("serial_no");
						String region_chi_name = rs.getString("region_chi_name");
					
						String dist_chi_name=rs.getString("dist_chi_name");
						String est_chi_name=rs.getString("est_chi_name");
						String phase_chi_name=rs.getString("phase_chi_name");
						
						String bldg_chi_name = rs.getString("bldg_chi_name");
						String floor = rs.getString("floor");
						String flat = rs.getString("flat");
						String gohome_serial_no =rs.getString("gohome_serial_no");
						
						
						if ( floor==null){
							floor="";
						}
						else if ( !floor.equals("") ){
							floor=floor.trim() + "樓";
						}
						if ( flat==null){
							flat="";
						}
						else if ( !flat.equals("") ){
							flat=flat.trim() + "室";
						}
					dist_chi_name=convertNull2Str(dist_chi_name);
					region_chi_name=convertNull2Str(region_chi_name);
					est_chi_name=convertNull2Str(est_chi_name); 
					phase_chi_name=convertNull2Str(phase_chi_name);
					bldg_chi_name=convertNull2Str(bldg_chi_name);
					floor=convertNull2Str(floor);
					flat=convertNull2Str(flat);
						
						String address = region_chi_name+dist_chi_name+est_chi_name+phase_chi_name+bldg_chi_name +floor+flat;
						address=convertNull2Str(address);
						String tx_Type = rs.getString("tx_type");
				     	
				    	 	
				        if (!(emp_id.equals(temp_emp_id))&& (tempSummary!="" )){
				        	sqlSerial_no+="\'dummy\'";
				        	finalSummary+="閣下已經有"+i+" 個樓盤列於GoHome.com.hk，樓盤資料如下 :\r\n\r\n";	
				        	finalSummary+=tempSummary;
				        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
							System.out.println("not send sqlSerial_no1: " + temp_emp_id);
							
							if (!isPro){
/*brian*/ 				   		 temp_emp_id="H1521962";
/*dan*/	//						temp_emp_id="H1110386";
/*vincent*/ //					temp_emp_id="H0422923";
								
							}
							System.out.println("send sqlSerial_no1: " + sqlSerial_no);
				        	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
				        	if (isPro){
					        	http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);
					        	http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);							  
								updateDB(sqlSerial_no,conn);
				        	}
				        	//System.out.println("sqlSerial_no1: " + sqlSerial_no);
				        	tempSummary="";
				        	System.out.println("send sqlSerial_no1: " + sqlSerial_no);
				        	sqlSerial_no="";
						    perStockDetail="";
						    finalSummary="";
						    i =0;
						
							} 
				        i++;
			        	 String type="";  
			        	 perStockDetail="";	 
			        	 
			        	 perStockDetail+=i+".樓盤\n";
			        	 perStockDetail+="- Stock ID: "+serialNumber+"\n";
			        	 perStockDetail+="- Stock Address: "+address+"\n";
			        	 	if ((tx_Type.equals("L")) ||(tx_Type=="L")){
			        	 		type="- 租\n";
			        	 	}else if ((tx_Type.equals("S")) ||(tx_Type=="S")){
			        	 		type="- 買\n";
			        	 	}else if ((tx_Type.equals("B")) ||(tx_Type=="B")){
			        	 		type="- 買 / 租 \n";
			        	 	}		
			        	perStockDetail+=type;	
			        	tempSummary+=perStockDetail;
			        	//sqlSerial_no+="\'"+serialNumber+"\',";
			        	sqlSerial_no+="\'"+gohome_serial_no+"\',";
			        	
				        temp_emp_id = emp_id;	
					  
				  }// end of while loop


				  // always send the last row
					if ((tempSummary!="")||!("".equals(tempSummary))){
						sqlSerial_no+="\'dummy\'";
			        	finalSummary+="閣下已經有"+i+" 個樓盤列於GoHome.com.hk，樓盤資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_emp_id);
						if (!isPro){
/*brian*/ 				    temp_emp_id="H1521962";
/*dan*/	//						temp_emp_id="H1110386";
/*vincent*/ //					temp_emp_id="H0422923";
						}	
						System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
			        	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
			          	if (isPro){
				        	http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);		
				        	http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);						
				        	updateDB(sqlSerial_no,conn);
			          	}
						tempSummary="";
			        	sqlSerial_no="";
					    perStockDetail="";
					    finalSummary="";
					    i =0;
						
					} 

				System.out.println("sendToSales end ");	
				// Do send to super
			//	getSendToSuper(conn);
				
        } catch(Exception e) {
			System.out.println("sendToSales ERROR");	
            e.printStackTrace();
        }
		finally {
//            PoolManager.closeResource(conn, stmt, rset);
			try{
				rs.close();
				conn.close();
			
			} catch(SQLException e) {
				System.out.println("SQLException");	
		        e.printStackTrace();
	        }
		
        }
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Utils


	
	private static void updateDB(String sqlSerial_no,Connection conn){
		
		PreparedStatement ps = null;
		ResultSet rs = null;	
	
		
		String query="UPDATE gohome_stock";	
		query+=" SET MSG_SENT_DATE = sysdate ";
		query+=" WHERE MSG_SENT_DATE is NULL and ";
		query+=" gohome_serial_no in(" + sqlSerial_no + ")" ;
	
		try{
			ps =  conn.prepareStatement(query);		
			rs = ps.executeQuery();		
			conn.commit();
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("SQL error");
		} finally{
			try {rs.close();} catch (SQLException e) {} 
			try {ps.close();} catch (SQLException e) {} 		
		}
	}	
		
	public int sendToSuper() {
		PreparedStatement ps =null;
		ResultSet rs = null;
	
		try{
			System.out.println("send to super start");
			URLConnection http = new URLConnection();
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");

			Connection conn = dataSource.getConnection();	
		
			String query="" ;

		query = //"select * from ( "+ // used to debug
				/* old version
				 * "select dhe.emp_id region_emp_id, t.dept_id, d.dept_code, d.chi_name, t.emp_id, t.chi_surname || t.chi_other_name emp_name,t. nickname||', '||t.eng_surname||' '||t.eng_other_name emp_name_eng, t.no_of_stock from "+
			
				" ( "+
				" select  "+
				" su.emp_id, "+
				" su.dept_id, "+
				" su.chi_surname, su.chi_other_name, "+
				" su.eng_surname,su.nickname,su.eng_other_name,"+
				" count(g.serial_no) as no_of_stock "+
				" from gohome_stock g "+
				" inner join stock_agents su on su.serial_no = g.serial_no "+
				" where "+
				" g.msg_sent_date is null "+
				" and g.gohome_serial_no is not null "+
				"and g.gohome_unpub_date is null "+
				" and exists ( "+
				" select 1 from stock s where g.serial_no = s.serial_no "+
				" ) "+
				" and exists( "+
				" select 1 from ( "+
				" select serial_no, min(post_type_seq) post_type_seq from stock_agents"+
				" group by serial_no "+
				" ) t1 where t1.serial_no = g.serial_no and t1.post_type_seq = su.post_type_seq "+
				" ) "+
				" group by su.emp_id, su.dept_id, su.chi_surname, su.chi_other_name ,su.eng_surname,su.nickname,su.eng_other_name"+
				" ) t  "+
				"inner join pis.super sp on trim(sp.dept_id) = t.dept_id and sp.status = 'N' and sysdate between sp.effect_date and nvl(sp. expire_date, sysdate) "+
				"inner join employee dhe on dhe.emp_id = sp.emp_id and dhe.grade_id in ('S1', 'S2') and dhe.emp_type = 'FL' and dhe.emp_sbu =  'ML' and dhe.dept_id <> '00165' "+
				" inner join department d on trim(d.dept_id) = trim(t.dept_id) and d.status = 'N' "+
				" order by dhe.emp_id, "+
				" d.dept_code, t.emp_id, no_of_stock ";
				*/
				
				"select dhe.emp_id region_emp_id, "+
				"       t.dept_id, "+
				"       t.dept_code, "+
				"       t.dept_chi_name, "+
				"       t.emp_id, "+
				"       t.chi_surname || t.chi_other_name emp_name, "+
				"       t. nickname || ', ' || t.eng_surname || ' ' || t.eng_other_name emp_name_eng, "+
				"       t.no_of_stock "+
				"  from (select su.emp_id, "+
				"               su.dept_id, "+
				"               su.chi_surname, "+
				"               su.chi_other_name, "+
				"               su.eng_surname, "+
				"               su.nickname, "+
				"               su.eng_other_name, "+
				"               su.dept_code,  "+
				"               su.dept_chi_name,                "+
				"               count(g.serial_no) as no_of_stock "+
				"          from gohome_stock g "+
				"         inner join stock_agents su on su.serial_no = g.serial_no "+
				"         where g.msg_sent_date is null "+
				"           and g.gohome_serial_no is not null "+
				"           and exists "+
				"         (select 1 from stock s where g.serial_no = s.serial_no) "+
				"           and exists "+
				"         (select 1 "+
				"                  from (select serial_no, min(post_type_seq) post_type_seq "+
				"                          from stock_agents "+
				"                         group by serial_no) t1 "+
				"                 where t1.serial_no = g.serial_no "+
				"                   and t1.post_type_seq = su.post_type_seq) "+
				"         group by su.emp_id, "+
				"                  su.dept_id, "+
				"                  su.chi_surname, "+
				"                  su.chi_other_name, "+
				"                  su.eng_surname, "+
				"                  su.nickname, "+
				"                  su.eng_other_name, "+
				"                  su.dept_code,  "+
				"                  su.dept_chi_name) t "+
				"inner join pis.super sp on trim(sp.dept_id) = t.dept_id "+
				"                        and sp.status = 'N' "+
				"                        and sysdate between sp.effect_date and "+
				"                            nvl(sp. expire_date, sysdate) "+
				"inner join employee dhe on dhe.emp_id = sp.emp_id "+
				"                        and sp.super_level > 400 and sp.super_level < 1800 "+
				"                        and dhe.emp_type = 'FL' "+
				"                        and dhe.emp_sbu = 'ML' "+
				"                        and dhe.dept_id <> '00165' "+
				"order by dhe.emp_id, t.dept_code, t.emp_id, no_of_stock ";

				//	") where region_emp_id ='H9608309'";//used to debug
			
		String temp_dept_id ="";
		String temp_super_id ="" ;
		
		String tempSummary ="";
		String tempBranch ="";
		
		String finalSummary ="";
		String perEmployeeDetail="";
		String temp_deptCode="";
		String temp_deptName="";
		String subject ="GoHome 分類廣告";
		String bc_code="MKT" ;	
		String sqlSerial_no="";
		String userName="市場部";
		
		String sendToIT = "H0422923";
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH)+1;		
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DATE);
		
		String sysdate = year+"年"+month+"月"+day+"日";
		sysdate = sysdate.trim();
		int no_of_branch=0;
	//	System.out.println("send to Super start ");		
			ps =  conn.prepareStatement(query);			
			rs = ps.executeQuery();		
			
			System.out.println("Start While loop");
			  while(rs.next()){
					
				  String displayName="";
				  String emp_id = rs.getString("EMP_ID").trim();
				  String dept_id = rs.getString("DEPT_ID").trim();
				  String super_id = rs.getString("REGION_EMP_ID").trim();
				  String deptCode = rs.getString("DEPT_CODE").trim();
				  String deptName = rs.getString("DEPT_CHI_NAME").trim();	
				  String engName = rs.getString("EMP_NAME_ENG");	
				
				  String chiName =  rs.getString("EMP_NAME")+" ";				  			  
				  
				  if (rs.wasNull()) {
					  	displayName=engName.trim();
				      } else {
				    	displayName=chiName.trim();
				      }
			
				  
				  String no_of_stock = rs.getString("no_of_stock");
				  
			        if (!(super_id.equals(temp_super_id))&& (tempSummary!="" )){
			        
			        	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com.hk，同事資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_super_id);
				//	System.out.println("temp_super_id "+temp_super_id+" temp_dept_id  "+temp_dept_id+"  temp_deptCode "+temp_deptCode);
						if (!isPro){
							temp_super_id="H1521962";
							System.out.println("in UAT loop");
						}	
						http.sendPost(subject, finalSummary, bc_code,temp_super_id, userName);
			        
						if (isPro){
		        			http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);	
		        			http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);	
						}	
			        	sqlSerial_no="\'"+temp_super_id+"\'";
			        	System.out.println("sqlSerial_no "+sqlSerial_no);
			
					//	System.out.println("temp_super_id: "+temp_super_id+"");
			        //	System.out.println("tempSummary "+tempSummary);
			        	System.out.println("(debug) sent to super");
			        	sqlSerial_no="";
			        	tempSummary="";
			        	tempBranch="";
			        	  finalSummary="";
			        	  no_of_branch=0;
					} 
			        perEmployeeDetail ="\t- "+displayName+" ("+emp_id+") 樓盤數量:"+no_of_stock+"\n";
			     //   perEmployeeDetail="emp_id: "+emp_id+" no_of_stock "+no_of_stock+"\n"; // for checking
			      
			        if(super_id.equals(temp_super_id)){	
			        //	System.out.println("Same Super ID");
				        	if(dept_id.equals(temp_dept_id)){	
				       // 		System.out.println("Same dept ID");
				        		tempBranch =perEmployeeDetail;
				        					        	
				        	}else {	
				    
				        		no_of_branch++;
				        		tempBranch  =no_of_branch+". "+deptCode+"  "+deptName+"\n" 
				        		+perEmployeeDetail;
				        					        				        		
				        	}							        	
			        }else {
			        //	System.out.println("differ Super ID");
			        	
			        	no_of_branch++;			        	
		        		tempBranch =no_of_branch+". "+deptCode+"  "+deptName+"\n"
		        		+perEmployeeDetail;	
			
			        }		
			        
			        tempSummary +=tempBranch;			    
			        temp_dept_id= dept_id;
			        temp_deptCode=deptCode;
        			temp_deptName=deptName;
        			temp_super_id=super_id;
	       
			  } // end of while loop
			  
			  //always do last 
			  if ((tempSummary!="" )){
		        	
				  	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com.hk，同事資料如下 :\r\n\r\n";	
		        	finalSummary+=tempSummary;
		        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
					System.out.println("not send sqlSerial_no1: " + temp_super_id);
			//	System.out.println("temp_super_id "+temp_super_id+" temp_dept_id  "+temp_dept_id+"  temp_deptCode "+temp_deptCode);
				//	temp_super_id="H1521962";
					if (!isPro){
						temp_super_id="H1521962";
						System.out.println("in UAT loop");
					}	
					http.sendPost(subject, finalSummary, bc_code,temp_super_id, userName);
		        
					if (isPro){
	        			http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);	
	        			http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);	
					}	
					
		        	sqlSerial_no="\'"+temp_super_id+"\'";
		        	System.out.println("sqlSerial_no "+sqlSerial_no);
		 
				//	System.out.println("temp_super_id: "+temp_super_id+"");
		        //	System.out.println("tempSummary "+tempSummary);
		        	System.out.println("(debug) sent to super");
		        	sqlSerial_no="";
		        	tempSummary="";
		        	  finalSummary="";
		        	  no_of_branch=0;
		       
				} 
			  

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch(Exception e) {
				
            e.printStackTrace();
        }
		finally{
			 System.out.println("send to super end");
			return 1;
		}		
	}
	

	public void sendToDeptHead(){
		PreparedStatement ps = null;
		ResultSet rs = null;	
		Connection conn = null;
	
		try {
			
			URLConnection http = new URLConnection();			
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");
		
			String query = 
							"select su.dept_id, d.dept_code, d.dept_head_id, su.emp_id,su.chi_surname || su.chi_other_name EMP_NAME, "+
							"su.nickname||', '||su.eng_surname||' '||su.eng_other_name EMP_NAME_ENG, "+
							"g.create_date, s.stock_id serial_no, s.tx_type, s.region_chi_name, s.dist_chi_name,   "+
							"s.est_chi_name, s.phase_chi_name, s.bldg_chi_name, s.floor, s.flat  "+
							"from gohome_stock g  "+
							"inner join stock s on g.serial_no = s.serial_no  "+
							"inner join stock_agents su on su.serial_no = s.serial_no   "+
							"inner join (   "+
							"select serial_no, min(post_type_seq) post_type_seq from stock_agents   "+
							"group by serial_no   "+
							") t on t.serial_no = g.serial_no and t.post_type_seq = su.post_type_seq   "+
							"inner join department d on trim(d.dept_id) = trim(su.dept_id) and d.status = 'N' "+
							"inner join employee e on e.emp_id = d.dept_head_id "+
							"where g.msg_sent_date is null  "+
							"and g.gohome_serial_no is not null  "+
							"and g.gohome_unpub_date is null "+
							"order by d.dept_head_id, d.dept_code, su.emp_id, serial_no ";
			
					
				conn = dataSource.getConnection();
				ps = conn.prepareStatement(query);			
				rs = ps.executeQuery();		
		
						
				String temp_emp_id="";
				String temp_dept_id="";
				String temp_dept_head_id="";
				String tempSummary="";
				String perStockDetail="";
				String finalSummary="";
				String subject ="GoHome 分類廣告";
				String bc_code="MKT" ;
			
				String sqlSerial_no="";
				String userName="市場部";
				String sendToIT = "H0422923";
				
				Calendar cal = Calendar.getInstance();
				int month = cal.get(Calendar.MONTH)+1;		
				int year = cal.get(Calendar.YEAR);
				int day = cal.get(Calendar.DATE);
				
				String sysdate = year+"年"+month+"月"+day+"日";
				sysdate = sysdate.trim();
				
				System.out.println("send deptHead start ");		
	
	
				 int i =0;
				  while(rs.next()){
					  	String emp_id = rs.getString("emp_id").trim();	
					  	String engName = rs.getString("EMP_NAME_ENG")+" ";							
						  String chiName =  rs.getString("EMP_NAME")+" ";				  			  
						  String displayName="";
						  if (rs.wasNull()) {
							  	displayName=engName.trim();
						      } else {
						    	displayName=chiName.trim();
						      }
					  	
					  	
						String serialNumber = rs.getString("serial_no");
						String region_chi_name = rs.getString("region_chi_name");
					
						String dist_chi_name=rs.getString("dist_chi_name");
						String est_chi_name=rs.getString("est_chi_name");
						String phase_chi_name=rs.getString("phase_chi_name");
						
						String bldg_chi_name = rs.getString("bldg_chi_name");
						String floor = rs.getString("floor");
						String flat = rs.getString("flat");
						//String gohome_serial_no =rs.getString("gohome_serial_no");
						String dept_id = rs.getString("dept_id");
						String dept_head_id = rs.getString("dept_head_id");
						
					
						if ( floor==null){
							floor="";
						}
						else if ( !floor.equals("") ){
							floor=floor.trim() + "樓";
						}
						if ( flat==null){
							flat="";
						}
						else if ( !flat.equals("") ){
							flat=flat.trim() + "室";
						}
										
						dist_chi_name=convertNull2Str(dist_chi_name);
						region_chi_name=convertNull2Str(region_chi_name);
						est_chi_name=convertNull2Str(est_chi_name);
						phase_chi_name=convertNull2Str(phase_chi_name);
						bldg_chi_name=convertNull2Str(bldg_chi_name);
						floor=convertNull2Str(floor);
						flat=convertNull2Str(flat);
						
						String address = est_chi_name+phase_chi_name+bldg_chi_name +floor+flat;
						address=address.trim();
						String tx_Type = rs.getString("tx_type");
				     	
				    	 	
				        if (!(dept_id.equals(temp_dept_id))&& (tempSummary!="" )){
				        	sqlSerial_no+="\'dummy\'";
				        //	finalSummary+="To "+temp_dept_head_id+"\n";
				        //	finalSummary+="集團策略性於GoHome.com.hk推出分類廣告，銳意擴闊客戶群，加強同事盤源的浸透性及增加成交機會，幫大家做盡市場。詳情請按下列連結。http://esfphoto.midland.com.hk/document/30/0000265830.pdf \r\n\r\n";				        	
				        	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com.hk，同事資料如下 :\r\n\r\n";	
				        	finalSummary+=tempSummary;
				        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
							System.out.println("not send sqlSerial_no1: " + temp_dept_head_id);
							if (!isPro){
	/*brian*/ 				   		temp_dept_head_id="H1521962";
	/*dan*/	//						temp_dept_head_id="H1110386";
	/*vincent*/ //					temp_dept_head_id="H0422923";
									System.out.println("in UAT loop");
							}
				//			System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
				        	http.sendPost(subject, finalSummary, bc_code,temp_dept_head_id, userName);
				        	
				         	if (isPro){
			        			http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);	
			        			http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);	
							}	
				        	System.out.println("sqlSerial_no1: " + sqlSerial_no);
				        	tempSummary="";
				        	sqlSerial_no="";
						    perStockDetail="";
						    finalSummary="";
						    i =0;
						
							} 
				        i++;
			        	 String type="";  
			        	 perStockDetail="";	 
			        	 
			        	 perStockDetail+=i+". "+address+"\n";
			        	 perStockDetail+="- Stock ID: "+serialNumber+"\n";			        	
			        	 perStockDetail+="- Agent: "+ displayName+ " ("+emp_id+")\n";
			        	 	if ((tx_Type.equals("L")) ||(tx_Type=="L")){
			        	 		type="- 租\n";
			        	 	}else if ((tx_Type.equals("S")) ||(tx_Type=="S")){
			        	 		type="- 買\n";
			        	 	}else if ((tx_Type.equals("B")) ||(tx_Type=="B")){
			        	 		type="- 買 / 租 \n";
			        	 	}		
			        	perStockDetail+=type;	
			        	tempSummary+=perStockDetail;
			        	//sqlSerial_no+="\'"+serialNumber+"\',";
			        //	sqlSerial_no+="\'"+gohome_serial_no+"\',";
			        	
				        temp_emp_id = emp_id;	
				        temp_dept_id = dept_id;
				        temp_dept_head_id=dept_head_id;
				  }// end of while loop
	
	
				  // always send the last row
					if ((tempSummary!="")||!("".equals(tempSummary))){
						sqlSerial_no+="\'dummy\'";
						//finalSummary+="To "+temp_dept_head_id+"\n";
					//	finalSummary+="集團策略性於GoHome.com.hk推出分類廣告，銳意擴闊客戶群，加強同事盤源的浸透性及增加成交機會，幫大家做盡市場。詳情請按下列連結。http://esfphoto.midland.com.hk/document/30/0000265830.pdf \r\n\r\n";
			        	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com.hk，同事資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_dept_head_id);
						if (!isPro){
/*brian*/ 				   		temp_dept_head_id="H1521962";
/*dan*/	//						temp_dept_head_id="H1110386";
/*vincent*/ //					temp_dept_head_id="H0422923";
								System.out.println("in UAT loop");
						}
			//			System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
			        	http.sendPost(subject, finalSummary, bc_code,temp_dept_head_id, userName);
			        	
			         	if (isPro){
		        			http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);	
		        			http.sendPostToIT(subject, finalSummary, bc_code,"H1521962", userName);	
						}	
			
			        	System.out.println("sqlSerial_no1: " + sqlSerial_no);
			        	tempSummary="";
			        	sqlSerial_no="";
					    perStockDetail="";
					    finalSummary="";
					    i =0;
						
					} 
	
				System.out.println("send to Dept Head end ");				
				
	    } catch(Exception e) {
			System.out.println("send to Dept Head error");	
	        e.printStackTrace();
	    }
	finally {
//  
		try{
			rs.close();
			conn.close();
		
		} catch(SQLException e) {
			System.out.println("send to Dept Head close error");	
	        e.printStackTrace();
        }		
    	} 
	}
	
	private String convertNull2Str(String input) {
		if (input == null) input = "";
		return input.trim();
	}
	
}
