package com.midland.msg;

import java.util.*;
import java.sql.*;
import java.lang.StringBuffer;

import javax.naming.*;
import javax.sql.*;

public class Msg {
	private Locale currentLocale = new Locale("zh", "HK");

	public Locale getCurrentLocale() {
		return this.currentLocale;
	}

	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}
	
	
	public int getSendToSalesKit() {
		
		PreparedStatement ps = null;
		ResultSet rs = null;	
		Connection conn = null;
		
	
		try {

			HttpURLConnectionExample http = new HttpURLConnectionExample();
			
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");
			
			String query = "select su.emp_id, g.create_date, s.serial_no,g.gohome_serial_no, s.tx_type, s.region_chi_name, s.dist_chi_name, " + 
				    "s.est_chi_name, s.phase_chi_name, s.bldg_chi_name, s.floor, s.flat " +
					"from gohome_stock g " +
					"inner join stock s on g.serial_no = s.serial_no " +
					"inner join stock_agents su on su.serial_no = s.serial_no " + 
					"inner join ( " + 
					"	 select serial_no, min(post_type_seq) post_type_seq from stock_agents " + 
					"	 group by serial_no " + 
					") t on t.serial_no = g.serial_no and t.post_type_seq = su.post_type_seq " + 
					"where g.msg_sent_date is null " +
					"	and g.gohome_serial_no is not null " +
					"order by su.emp_id desc ";
			
				conn = dataSource.getConnection();
				ps = conn.prepareStatement(query);			
				rs = ps.executeQuery();		
		
							
				String temp_emp_id="";
				String tempSummary="";
				String perStockDetail="";
				String finalSummary="";
				String subject ="GoHome 分類廣告";
				String bc_code="IT_SYS" ;
				String emp_id="";
				String sqlSerial_no="";
				String userName="資訊科技部";
				String sendToIT = "H1521962";

				System.out.println("send start ");		


				 int i =0;
				  while(rs.next()){
					  emp_id = rs.getString("emp_id");				
						String serialNumber = rs.getString("serial_no");
						String region_chi_name = rs.getString("region_chi_name");
					
						String dist_chi_name=rs.getString("dist_chi_name");
						String est_chi_name=rs.getString("est_chi_name");
						String phase_chi_name=rs.getString("phase_chi_name");
						
						String bldg_chi_name = rs.getString("bldg_chi_name");
						String floor = rs.getString("floor");
						String flat = rs.getString("flat");
						String gohome_serial_no =rs.getString("gohome_serial_no");
						
						if ( est_chi_name==null){
							est_chi_name="";
						}				
						if ( phase_chi_name==null){
							phase_chi_name="";
						}
						if ( bldg_chi_name==null){
							bldg_chi_name="";
						}
						if ( floor==null){
							floor="";
						}
						else if ( !floor.equals("") ){
							floor=floor.trim() + "樓";
						}
						if ( flat==null){
							flat="";
						}
						else if ( !flat.equals("") ){
							flat=flat.trim() + "室";
						}
						dist_chi_name=dist_chi_name.trim();
						region_chi_name=region_chi_name.trim();
						est_chi_name=est_chi_name.trim();
						phase_chi_name=phase_chi_name.trim();
						bldg_chi_name=bldg_chi_name.trim();
						floor=floor.trim();
						flat=flat.trim();
						
						String address = region_chi_name+dist_chi_name+est_chi_name+phase_chi_name+bldg_chi_name +floor+flat;
						address=address.trim();
						String tx_Type = rs.getString("tx_type");
				     	
				    	 	
				        if (!(emp_id.equals(temp_emp_id))&& (tempSummary!="" )){
				        	sqlSerial_no+="\'dummy\'";
				        	finalSummary+="閣下已經有"+i+" 個樓盤列於GoHome.com，樓盤資料如下 :\r\n\r\n";	
				        	finalSummary+=tempSummary;
				        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
							System.out.println("not send sqlSerial_no1: " + temp_emp_id);
/*brian*/ //				    temp_emp_id="H1521962";
/*dan*/	//						temp_emp_id="H1110386";
/*vincent*/ //					temp_emp_id="H0422923";
							System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
				     //   	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
					//   	http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);
							   
							//    	updateDB(sqlSerial_no,conn);
				        	System.out.println("sqlSerial_no1: " + sqlSerial_no);
				        	tempSummary="";
				        	System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
				        	sqlSerial_no="";
						    perStockDetail="";
						    finalSummary="";
						    i =0;
						
							} 
				        i++;
			        	 String type="";  
			        	 perStockDetail="";	 
			        	 
			        	 perStockDetail+=i+".樓盤\n";
			        	 perStockDetail+="- Stock ID: "+serialNumber+"\n";
			        	 perStockDetail+="- Stock Address: "+address+"\n";
			        	 	if ((tx_Type.equals("L")) ||(tx_Type=="L")){
			        	 		type="- 租\n";
			        	 	}else if ((tx_Type.equals("S")) ||(tx_Type=="S")){
			        	 		type="- 買\n";
			        	 	}else if ((tx_Type.equals("B")) ||(tx_Type=="B")){
			        	 		type="- 買 / 租 \n";
			        	 	}		
			        	perStockDetail+=type;	
			        	tempSummary+=perStockDetail;
			        	//sqlSerial_no+="\'"+serialNumber+"\',";
			        	sqlSerial_no+="\'"+gohome_serial_no+"\',";
			        	
				        temp_emp_id = emp_id;	
					  
				  }// end of while loop


				  // always send the last row
					if ((tempSummary!="")||!("".equals(tempSummary))){
						sqlSerial_no+="\'dummy\'";
			        	finalSummary+="閣下已經有"+i+" 個樓盤列於GoHome.com，樓盤資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_emp_id);
/*brian*/ //				    temp_emp_id="H1521962";
/*dan*/	//						temp_emp_id="H1110386";
/*vincent*/ //					temp_emp_id="H0422923";
						System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
			     //   	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
				//		http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);		
				//		updateDB(sqlSerial_no,conn);
						System.out.println("sqlSerial_no2: " + sqlSerial_no);
			        	tempSummary="";
			        	sqlSerial_no="";
					    perStockDetail="";
					    finalSummary="";
					    i =0;
						
					} 

				System.out.println("send end ");	
				// Do send to super
			//	getSendToSuper(conn);
				
        } catch(Exception e) {
			System.out.println("getSendToSalesKit");	
            e.printStackTrace();
        }
		finally {
//            PoolManager.closeResource(conn, stmt, rset);
			try{
				rs.close();
				conn.close();
			
			} catch(SQLException e) {
				System.out.println("SQLException");	
		        e.printStackTrace();
	        }
			return 1;
        }
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Utils

	private String convertNull2Str(String input) {
		if (input == null)
			input = "";
		return input.trim();
	}
	
	private static void updateDB(String sqlSerial_no,Connection conn){
		
		PreparedStatement ps = null;
		ResultSet rs = null;	
	
		
		String query="UPDATE gohome_stock";
		query+="_test";
		query+=" SET MSG_SENT_DATE = sysdate ";
		query+=" WHERE MSG_SENT_DATE is NULL and ";
		query+=" gohome_serial_no in(" + sqlSerial_no + ")" ;
	
		try{
			ps =  conn.prepareStatement(query);		
			rs = ps.executeQuery();		
			conn.commit();
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("SQL error");
		} finally{
			try {rs.close();} catch (SQLException e) {} 
			try {ps.close();} catch (SQLException e) {} 		
		}
	}
	
	private static void updateDBSuper(String serial_no,Connection conn){
		
		PreparedStatement ps = null;
		ResultSet rs = null;
	
		String query="UPDATE gohome_stock";
		query+="_test";
		query+=" SET REGION_HEAD_MSG_SENT_DATE = sysdate ";
		query+=" WHERE ";
		query+=" serial_no in(" + serial_no + ")" ;
	
		try{
			ps =  conn.prepareStatement(query);		
			rs = ps.executeQuery();		
			conn.commit();
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("SQL error");
		} finally{
			try {rs.close();} catch (SQLException e) {} 
			try {ps.close();} catch (SQLException e) {} 		
		}
	}
	
	public int SendToSuper() {
		PreparedStatement ps =null;
		ResultSet rs = null;
	
		try{
			System.out.println("send to super start");
			HttpURLConnectionExample http = new HttpURLConnectionExample();
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");

			Connection conn = dataSource.getConnection();	
			
		
			String query="" ;

		query = //"select * from ( "+ // used to debug
				"select dhe.emp_id region_emp_id, t.dept_id, d.dept_code, d.chi_name, t.emp_id, t.chi_surname || t.chi_other_name emp_name,t. nickname||', '||t.eng_surname||' '||t.eng_other_name emp_name_eng, t.no_of_stock from "+
				" ( "+
				" select  "+
				" su.emp_id, "+
				" su.dept_id, "+
				" su.chi_surname, su.chi_other_name, "+
				" su.eng_surname,su.nickname,su.eng_other_name,"+
				" count(g.serial_no) as no_of_stock "+
				" from gohome_stock g "+
				" inner join stock_agents su on su.serial_no = g.serial_no "+
				" where "+
				" g.msg_sent_date is not null "+
				" and g.gohome_serial_no is not null "+
				" and g.REGION_HEAD_MSG_SENT_DATE is null "+
				" and exists ( "+
				" select 1 from stock s where g.serial_no = s.serial_no "+
				" ) "+
				" and exists( "+
				" select 1 from ( "+
				" select serial_no, min(post_type_seq) post_type_seq from stock_agents"+
				" group by serial_no "+
				" ) t1 where t1.serial_no = g.serial_no and t1.post_type_seq = su.post_type_seq "+
				" ) "+
				" group by su.emp_id, su.dept_id, su.chi_surname, su.chi_other_name ,su.eng_surname,su.nickname,su.eng_other_name"+
				" ) t  "+
				"inner join pis.super sp on trim(sp.dept_id) = t.dept_id and sp.status = 'N' and sysdate between sp.effect_date and nvl(sp. expire_date, sysdate) "+
				"inner join employee dhe on dhe.emp_id = sp.emp_id and dhe.grade_id in ('S1', 'S2') and dhe.emp_type = 'FL' and dhe.emp_sbu =  'ML' and dhe.dept_id <> '00165' "+
				" inner join department d on trim(d.dept_id) = trim(t.dept_id) and d.status = 'N' "+
				" order by dhe.emp_id, "+
				" d.dept_code, t.emp_id, no_of_stock ";
				//	") where region_emp_id ='H9608309'";//used to debug
			
		String temp_dept_id ="";
		String temp_super_id ="" ;
		
		String tempSummary ="";
		String tempBranch ="";
		
		String finalSummary ="";
		String perEmployeeDetail="";
		String temp_deptCode="";
		String temp_deptName="";
		String subject ="GoHome 分類廣告";
		String bc_code="IT_SYS" ;	
		String sqlSerial_no="";
		String userName="資訊科技部";
		String sendToIT = "H1521962";
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH)+1;		
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DATE);
		
		String sysdate = year+"年"+month+"月"+day+"日";
		sysdate = sysdate.trim();
		int no_of_branch=0;
		System.out.println("getSendToSalesKit executeQuery 1");		
			ps =  conn.prepareStatement(query);			
			rs = ps.executeQuery();		
			
			System.out.println("Start While loop");
			  while(rs.next()){
					
				  String displayName="";
				  String emp_id = rs.getString("EMP_ID").trim();
				  String dept_id = rs.getString("DEPT_ID").trim();
				  String super_id = rs.getString("REGION_EMP_ID").trim();
				  String deptCode = rs.getString("DEPT_CODE").trim();
				  String deptName = rs.getString("CHI_NAME").trim();	
				  String engName = rs.getString("EMP_NAME_ENG");	
				
				  String chiName =  rs.getString("EMP_NAME")+" ";				  			  
				  
				  if (rs.wasNull()) {
					  	displayName=engName.trim();
				      } else {
				    	displayName=chiName.trim();
				      }
			
				  
				  String no_of_stock = rs.getString("no_of_stock");
				  
			        if (!(super_id.equals(temp_super_id))&& (tempSummary!="" )){
			        
			        	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com，同事資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_super_id);
				//	System.out.println("temp_super_id "+temp_super_id+" temp_dept_id  "+temp_dept_id+"  temp_deptCode "+temp_deptCode);
						temp_super_id="H1521962";
			        //	http.sendPost(subject, finalSummary, bc_code,temp_super_id, userName);
			        	http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);
			        	sqlSerial_no="\'"+temp_super_id+"\'";
			        	System.out.println("sqlSerial_no "+sqlSerial_no);
			    //    	updateDBSuper(sqlSerial_no,conn);
					//	System.out.println("temp_super_id: "+temp_super_id+"");
			        //	System.out.println("tempSummary "+tempSummary);
			        	System.out.println("(debug) sent to super");
			        	sqlSerial_no="";
			        	tempSummary="";
			        	tempBranch="";
			        	  finalSummary="";
			        	  no_of_branch=0;
					} 
			        perEmployeeDetail ="\t- "+displayName+" ("+emp_id+") 樓盤數量:"+no_of_stock+"\n";
			     //   perEmployeeDetail="emp_id: "+emp_id+" no_of_stock "+no_of_stock+"\n"; // for checking
			      
			        if(super_id.equals(temp_super_id)){	
			        //	System.out.println("Same Super ID");
				        	if(dept_id.equals(temp_dept_id)){	
				       // 		System.out.println("Same dept ID");
				        		tempBranch =perEmployeeDetail;
				        					        	
				        	}else {	
				    
				        		no_of_branch++;
				        		tempBranch  =no_of_branch+". "+deptCode+"  "+deptName+"\n" 
				        		+perEmployeeDetail;
				        					        				        		
				        	}							        	
			        }else {
			        //	System.out.println("differ Super ID");
			        	
			        	no_of_branch++;
			        	
		        		tempBranch =no_of_branch+". "+deptCode+"  "+deptName+"\n"
		        		+perEmployeeDetail;	
			
			        }		
			        
			        tempSummary +=tempBranch;			    
			        temp_dept_id= dept_id;
			        temp_deptCode=deptCode;
        			temp_deptName=deptName;
        			temp_super_id=super_id;
	       
			  } // end of while loop
			  
			  //always do last 
			  if ((tempSummary!="" )){
		        	
				  	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com，同事資料如下 :\r\n\r\n";	
		        	finalSummary+=tempSummary;
		        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
					System.out.println("not send sqlSerial_no1: " + temp_super_id);
			//	System.out.println("temp_super_id "+temp_super_id+" temp_dept_id  "+temp_dept_id+"  temp_deptCode "+temp_deptCode);
					temp_super_id="H1521962";
		     //   	http.sendPost(subject, finalSummary, bc_code,temp_super_id, userName);
					
					http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);
		        	sqlSerial_no="\'"+temp_super_id+"\'";
		        	System.out.println("sqlSerial_no "+sqlSerial_no);
		    //    	updateDBSuper(sqlSerial_no,conn);
				//	System.out.println("temp_super_id: "+temp_super_id+"");
		        //	System.out.println("tempSummary "+tempSummary);
		        	System.out.println("(debug) sent to super");
		        	sqlSerial_no="";
		        	tempSummary="";
		        	  finalSummary="";
		        	  no_of_branch=0;
		       
				} 
			  

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch(Exception e) {
				
            e.printStackTrace();
        }
		finally{
			 System.out.println("send to super end");
			 return 1;
		}		
	}
	

	public int sendToDeptHead(){
		PreparedStatement ps = null;
		ResultSet rs = null;	
		Connection conn = null;
		
	
		try {
	
			HttpURLConnectionExample http = new HttpURLConnectionExample();			
			Context initContext  = new InitialContext();
			DataSource dataSource = (DataSource)initContext.lookup("java:comp/env/jdbc/db1/digital");
			
			String query = "select su.emp_id, g.create_date, s.serial_no,g.gohome_serial_no, s.tx_type, s.region_chi_name, s.dist_chi_name, " + 
				    "s.est_chi_name, s.phase_chi_name, s.bldg_chi_name, s.floor, s.flat " +
					"from gohome_stock g " +
					"inner join stock s on g.serial_no = s.serial_no " +
					"inner join stock_agents su on su.serial_no = s.serial_no " + 
					"inner join ( " + 
					"	 select serial_no, min(post_type_seq) post_type_seq from stock_agents " + 
					"	 group by serial_no " + 
					") t on t.serial_no = g.serial_no and t.post_type_seq = su.post_type_seq " + 
					"where g.msg_sent_date is null " +
					"	and g.gohome_serial_no is not null " +
					"order by su.emp_id desc ";
			
				conn = dataSource.getConnection();
				ps = conn.prepareStatement(query);			
				rs = ps.executeQuery();		
		
							
				String temp_emp_id="";
				String tempSummary="";
				String perStockDetail="";
				String finalSummary="";
				String subject ="GoHome 分類廣告";
				String bc_code="IT_SYS" ;
				String emp_id="";
				String sqlSerial_no="";
				String userName="資訊科技部";
				String sendToIT = "H1521962";
				
				Calendar cal = Calendar.getInstance();
				int month = cal.get(Calendar.MONTH)+1;		
				int year = cal.get(Calendar.YEAR);
				int day = cal.get(Calendar.DATE);
				
				String sysdate = year+"年"+month+"月"+day+"日";
				sysdate = sysdate.trim();
				
				System.out.println("send deptHead start ");		
	
	
				 int i =0;
				  while(rs.next()){
					  emp_id = rs.getString("emp_id");				
						String serialNumber = rs.getString("serial_no");
						String region_chi_name = rs.getString("region_chi_name");
					
						String dist_chi_name=rs.getString("dist_chi_name");
						String est_chi_name=rs.getString("est_chi_name");
						String phase_chi_name=rs.getString("phase_chi_name");
						
						String bldg_chi_name = rs.getString("bldg_chi_name");
						String floor = rs.getString("floor");
						String flat = rs.getString("flat");
						String gohome_serial_no =rs.getString("gohome_serial_no");
						
						if ( est_chi_name==null){
							est_chi_name="";
						}				
						if ( phase_chi_name==null){
							phase_chi_name="";
						}
						if ( bldg_chi_name==null){
							bldg_chi_name="";
						}
						if ( floor==null){
							floor="";
						}
						else if ( !floor.equals("") ){
							floor=floor.trim() + "樓";
						}
						if ( flat==null){
							flat="";
						}
						else if ( !flat.equals("") ){
							flat=flat.trim() + "室";
						}
						dist_chi_name=dist_chi_name.trim();
						region_chi_name=region_chi_name.trim();
						est_chi_name=est_chi_name.trim();
						phase_chi_name=phase_chi_name.trim();
						bldg_chi_name=bldg_chi_name.trim();
						floor=floor.trim();
						flat=flat.trim();
						
						String address = region_chi_name+dist_chi_name+est_chi_name+phase_chi_name+bldg_chi_name +floor+flat;
						address=address.trim();
						String tx_Type = rs.getString("tx_type");
				     	
				    	 	
				        if (!(emp_id.equals(temp_emp_id))&& (tempSummary!="" )){
				        	sqlSerial_no+="\'dummy\'";
				        	finalSummary+="閣下管轄的同事在"+sysdate+"，已經有樓盤列於GoHome.com，同事資料如下 :\r\n\r\n";	
				        	finalSummary+=tempSummary;
				        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
							System.out.println("not send sqlSerial_no1: " + temp_emp_id);
	/*brian*/ 				    temp_emp_id="H1521962";
	/*dan*/	//						temp_emp_id="H1110386";
	/*vincent*/ //					temp_emp_id="H0422923";
							System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
				        	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
					//   	http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);
							   
							//    	updateDB(sqlSerial_no,conn);
				        	System.out.println("sqlSerial_no1: " + sqlSerial_no);
				        	tempSummary="";
				        	System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
				        	sqlSerial_no="";
						    perStockDetail="";
						    finalSummary="";
						    i =0;
						
							} 
				        i++;
			        	 String type="";  
			        	 perStockDetail="";	 
			        	 
			        	 perStockDetail+=i+".員工名 ("+"ID" +")\n";
			        	 perStockDetail+="\t- Stock ID: "+serialNumber+"\n";
			        	 perStockDetail+="\t- Stock Address: "+address+"\n";
			        	 	if ((tx_Type.equals("L")) ||(tx_Type=="L")){
			        	 		type="\t- 租\n";
			        	 	}else if ((tx_Type.equals("S")) ||(tx_Type=="S")){
			        	 		type="\t- 買\n";
			        	 	}else if ((tx_Type.equals("B")) ||(tx_Type=="B")){
			        	 		type="\t- 買 / 租 \n";
			        	 	}		
			        	perStockDetail+=type;	
			        	tempSummary+=perStockDetail;
			        	//sqlSerial_no+="\'"+serialNumber+"\',";
			        	sqlSerial_no+="\'"+gohome_serial_no+"\',";
			        	
				        temp_emp_id = emp_id;	
					  
				  }// end of while loop
	
	
				  // always send the last row
					if ((tempSummary!="")||!("".equals(tempSummary))){
						sqlSerial_no+="\'dummy\'";
			        	finalSummary+="閣下已經有"+i+" 個樓盤列於GoHome.com，樓盤資料如下 :\r\n\r\n";	
			        	finalSummary+=tempSummary;
			        	finalSummary+="\r\n如有任何查詢，歡迎致電IT熱線 2273-6666與我們聯絡。";	
						System.out.println("not send sqlSerial_no1: " + temp_emp_id);
	/*brian*/ 				    temp_emp_id="H1521962";
	/*dan*/	//						temp_emp_id="H1110386";
	/*vincent*/ //					temp_emp_id="H0422923";
						System.out.println("not send sqlSerial_no1: " + sqlSerial_no);
			       	http.sendPost(subject, finalSummary, bc_code, temp_emp_id, userName);
				//		http.sendPostToIT(subject, finalSummary, bc_code,sendToIT, userName);		
				//		updateDB(sqlSerial_no,conn);
						System.out.println("sqlSerial_no2: " + sqlSerial_no);
			        	tempSummary="";
			        	sqlSerial_no="";
					    perStockDetail="";
					    finalSummary="";
					    i =0;
						
					} 
	
				System.out.println("send end ");	
				// Do send to super
			//	getSendToSuper(conn);
				
	    } catch(Exception e) {
			System.out.println("getSendToSalesKit");	
	        e.printStackTrace();
	    }
	finally {
//        PoolManager.closeResource(conn, stmt, rset);
		try{
			rs.close();
			conn.close();
		
		} catch(SQLException e) {
			System.out.println("SQLException");	
	        e.printStackTrace();
        }
		return 1;
    	}
	}
	
	public int test11(){
		System.out.println("tseting 11111");
		return 1;
	}
}
